# SBB train arrival analysis

This project analyzes the delay of swiss trains from SBB upon arrival in the station.

The main focus is on data exploration of delays with visual and statistical tools.

A publicly available dataset from SBB is used from an API that publishes the arrival
and departure times of all trains from the previous day. 
Note that the underlying dataset will update when the notebook is run and  might not 
represent the written text in the cells.

The project is composed of the main analysis in the notebook 
`SBB - Daily delays analysis.ipynb`, some functions in the module `sbbarrivals.py` and 
the corresponding tests in `test_sbbarrivals`
