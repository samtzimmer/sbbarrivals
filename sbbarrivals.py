"""
SBB arrivals module for the SBB - Daily delays visual
"""


import os
import sys
import datetime
from typing import List, Optional, Tuple
from zipfile import ZipFile
import logging

import geopandas as gpd
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import requests
from matplotlib.lines import Line2D
from matplotlib.colors import ListedColormap, to_rgb


# Logging
logger = logging.Logger(__name__)
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler(sys.stdout))


def get_linear_color_map(
    c1, c2, alpha: Optional[Tuple[float, float]] = (1, 1), n: Optional[int] = 100
) -> ListedColormap:
    """Create a linear colormap between two colors.

    :param c1: Low value color
    :param c2: High value color
    :param alpha: Transparency values (optional)
    :param n: Number of interpolated values (optional)
    :return: Colormap
    """
    # Colors to RGB
    c1 = to_rgb(c1)
    c2 = to_rgb(c2)

    # Discretize colors
    colors = np.c_[
        np.linspace(c1[0], c2[0], n),
        np.linspace(c1[1], c2[1], n),
        np.linspace(c1[2], c2[2], n),
        np.linspace(alpha[0], alpha[1], n),
    ]

    # Color map
    cmap = ListedColormap(colors)
    return cmap


def colormap(c1, c2, threshold, n):
    c1 = to_rgb(c1)
    c2 = to_rgb(c2)
    # Build the colors
    colors = np.zeros((n, 4))
    colors[:threshold, :] = np.r_[c1, 0.4]
    colors[threshold:, :] = np.r_[c2, 1]
    return ListedColormap(colors)


def plot_train_station_stats(
    stats: pd.Series,
    gmap: gpd.GeoDataFrame,
    ax: Optional[plt.Axes] = None,
    timestamp: Optional[str] = None,
    datasource: Optional[str] = None,
    legend_values: Optional[List[int]] = (1, 5, 10, 15),
    legend_lims: Optional[Tuple[float, float]] = (1, 6),
    legend_title: Optional[str] = "",
    color: Optional[str] = "C0",
    text_color: Optional[str] = "black",
    gmap_color: Optional[str] = "lightgray",
    bg_color: Optional[str] = None,
    cmap: Optional[ListedColormap] = None,
    fontsize: Optional[float] = 9,
    value_range: Optional[Tuple[float, float]] = None,
) -> Tuple[plt.Figure, plt.Axes]:
    """Plot the statistics of train stations on a map.

    :param stats: Pandas Series with multiindex (`station name`, `latitude`, `longitude`)
                  and value of the statistic
    :param gmap: GeoPanas DataFrame with map
    :param ax: Matplotlib axis to plot on (optional)
    :param timestamp: Timestamp of the data (optional)
    :param datasource: Reference to the data source (optional)
    :param legend_values: Displayed nice values in the legend (optional)
    :param legend_lims: Min and max size of the datapoints in the figure (optional)
    :param legend_title: Text describing the datapoints in the legend (optional)
    :param color: Theme color of the graph (optional)
    :param text_color: Text color (optional)
    :param gmap_color: Background color of the map (optional)
    :param bg_color: Figure background color (optional)
    :param cmap: Define a colormap for the data values. By default, `color` is used (optional)
    :param fontsize: Font size of regular text (optional)
    :param value_range: Expected value range of the data (optional)
    :return: Axes with plt Artists
    """
    # Create a colormap
    if cmap is None:
        cmap = lambda x: color

    # Value range
    if value_range is None:
        value_range = [min(stats), max(stats)]

    if ax is None:
        fig, ax = plt.subplots(tight_layout=True)
    else:
        fig = ax.get_figure()

    # Draw map
    gmap.plot(color=gmap_color, ax=ax)

    to_markersize = lambda x: (x - min(value_range)) / (
        max(value_range) - min(value_range)
    ) * (max(legend_lims) - min(legend_lims)) + min(legend_lims)

    # Plot data points
    for (station, lat, lon), value in stats.items():
        ax.plot(
            lon,
            lat,
            marker="o",
            ms=to_markersize(value),
            c=cmap(value / max(value_range)),
            markeredgecolor="none",
        )
    ax.set_axis_off()

    # Legend handles
    handles = [
        Line2D(
            [0],
            [0],
            color=cmap(value / max(value_range)),
            markeredgecolor="none",
            ms=to_markersize(value),
            marker="o",
            ls="none",
            label=f"{value:d}",
        )
        for value in reversed(legend_values)
    ]

    # Legend
    legend = ax.legend(
        handles=handles,
        loc="upper right",
        frameon=False,
        title=legend_title,
        fontsize=fontsize,
        title_fontsize=fontsize * 1.1,
        labelcolor=text_color,
    )
    plt.setp(legend.get_title(), color=text_color)

    # Add timestamp
    if timestamp is not None:
        ax.text(
            x=0.07,
            y=0.93,
            s=f"{timestamp}",
            transform=fig.transFigure,
            ha="left",
            va="top",
            color=text_color,
            fontsize=fontsize * 1.1,
        )

    # Add data source
    if datasource is not None:
        ax.text(
            x=0.07,
            y=0.07,
            s=datasource,
            transform=fig.transFigure,
            ha="left",
            va="top",
            color=text_color,
            fontsize=fontsize,
        )

    if bg_color is not None:
        fig.set_facecolor(bg_color)

    return fig, ax


def get_zip_content(url: str) -> List[str]:
    """Download a zip file and extract its content.

    .. note:: Downloaded zip file is deleted after unpacking

    :param url: Url to the zip file
    :return: Paths of the extracted files
    """

    tmpfile = "tmpfile.zip"

    # Download zip
    with open(tmpfile, "wb") as io_stream:
        io_stream.write(requests.get(url).content)

    # Extract zip
    with ZipFile(tmpfile) as zipfile:
        out_files = zipfile.namelist()
        zipfile.extractall()

    # Remove temporary file
    os.remove(tmpfile)

    return out_files


def daylight_brightness(timestamp: datetime) -> float:
    """Return the daylight brightness as a float.

    This function is a good guess and does not take any astronomical effects into
    account.

    :param timestamp: Timestamp of the observation.
    :return: Brightness value.
    """
    time = timestamp.time()
    since_midnight = datetime.timedelta(
        hours=time.hour, minutes=time.minute, seconds=time.second
    )
    fraction_of_day = since_midnight / datetime.timedelta(days=1)

    return np.sin(fraction_of_day * np.pi) ** 2
